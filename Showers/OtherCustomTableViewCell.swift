//
//  OtherCustomTableViewCell.swift
//  Showers
//
//  Created by dilip chaurasiya on 5/15/16.
//  Copyright © 2016 dilip chaurasiya. All rights reserved.
//

import UIKit

class OtherCustomTableViewCell: UITableViewCell {

    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var weatherCondition: UILabel!
    @IBOutlet weak var low: UILabel!
    @IBOutlet weak var high: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
