//
//  CustomTableViewCell.swift
//  Showers
//
//  Created by dilip chaurasiya on 5/14/16.
//  Copyright © 2016 dilip chaurasiya. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var weatherCondition: UILabel!
    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var low: UILabel!
    @IBOutlet weak var high: UILabel!

    @IBOutlet weak var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
