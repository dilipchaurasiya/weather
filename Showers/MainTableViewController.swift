//
//  MainTableViewController.swift
//  Showers
//
//  Created by dilip chaurasiya on 5/13/16.
//  Copyright © 2016 dilip chaurasiya. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage //for efficient image loading

class MainTableViewController: UITableViewController {
    
    let celcius = "\u{00B0}" //for Celcius Symbol
    var result: NSDictionary! //dictionary to store respose result
    var imagedictionary : NSDictionary!
    var imageUrlString: NSString!
    var networkStatus: NSString! //status of internet connectivity
    var forcast: NSMutableArray!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    
    // if result is not recieved show only 1 row otherwise show all the rows
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if(self.result != nil)
        {
            return (self.result.valueForKeyPath("query.results.channel.item.forecast")?.count)!
         
        }
        else
        {
        return 1
        }
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        // if result is received show all the result otherwise show 1 row containing proper message ie. if loading then show loading.. message or if error occured show error message
        if(self.result != nil)
        {
            // Configure the cell...
            forcast = self.result.valueForKeyPath("query.results.channel.item.forecast") as! NSMutableArray
            let imagecode: String = forcast?.objectAtIndex(indexPath.row).objectForKey("code") as! String
            
            if(indexPath.row == 0)
            {
                //cell of type CustomTableViewCell for current date result
                let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! CustomTableViewCell

                cell.backgroundColor = UIColor.lightGrayColor()
                
                //SDWebImage method
                cell.weatherImage?.sd_setImageWithURL(NSURL(string: "http://l.yimg.com/a/i/us/we/52/"+imagecode+".gif"), placeholderImage: UIImage(named: "placeholderImage"))
                
                cell.weatherCondition.text = forcast?.objectAtIndex(indexPath.row).objectForKey("text") as? String
                
                cell.temperature.text = NSString(format:"%@%@", (self.result.valueForKeyPath("query.results.channel.item.condition.temp") as? String)!, celcius) as String
                
                cell.day.text = forcast?.objectAtIndex(indexPath.row).objectForKey("day") as? String
                
                cell.low.text = NSString(format:"%@%@", (forcast?.objectAtIndex(indexPath.row).objectForKey("low") as? String)!, celcius) as String
                
                cell.high.text = NSString(format:"%@%@", (forcast?.objectAtIndex(indexPath.row).objectForKey("high") as? String)!, celcius) as String
                
                cell.date.text = forcast?.objectAtIndex(indexPath.row).objectForKey("date") as? String
                
                return cell
            }
            else
            {
                //cell of type OtherCustomTableViewCell for other day results
                let cell = tableView.dequeueReusableCellWithIdentifier("cell1") as! OtherCustomTableViewCell
                
                //SDWebImage method
                cell.weatherImage?.sd_setImageWithURL(NSURL(string: "http://l.yimg.com/a/i/us/we/52/"+imagecode+".gif"), placeholderImage: UIImage(named: "placeholderImage"))
            
                cell.weatherCondition.text = forcast?.objectAtIndex(indexPath.row).objectForKey("text") as? String
            
                cell.day.text = forcast?.objectAtIndex(indexPath.row).objectForKey("day") as? String
            
                cell.low.text = NSString(format:"%@%@", (forcast?.objectAtIndex(indexPath.row).objectForKey("low") as? String)!, celcius) as String
            
                cell.high.text = NSString(format:"%@%@", (forcast?.objectAtIndex(indexPath.row).objectForKey("high") as? String)!, celcius) as String
            
                return cell
            }
        }
        else
        {
            // if error occured show erroe message else loading
            let cell = tableView.dequeueReusableCellWithIdentifier("loading", forIndexPath: indexPath)
            
            if(networkStatus != nil && networkStatus == "Failed")
            {
                cell.textLabel?.text = "Could not load data"
            }
            else{
                cell.textLabel?.text = "Loading...."
            }
            return cell
        }
        
    }
    
    // if first row row height increased
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        var height: CGFloat
        if(indexPath.row == 0)
        {
            height = self.view.frame.size.height/2;
        }
        else
        {
            height = 70;
        }
        return height;
    }
    
    // for displaying details of selected date
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let detailView = self.storyboard!.instantiateViewControllerWithIdentifier("detail") as! DetailViewController
        
        detailView.forcast = forcast?.objectAtIndex(indexPath.row) as! NSDictionary
        if(indexPath.row == 0)
        {
            detailView.atmosphere = self.result.valueForKeyPath("query.results.channel.atmosphere") as! NSDictionary
            detailView.wind = self.result.valueForKeyPath("query.results.channel.wind") as! NSDictionary
        }
        
        let navController = UINavigationController(rootViewController: detailView)
        self.presentViewController(navController, animated: true, completion: nil)
    
        
    }
    
    // for recieving data from appDelegate and assigning data to self.result
    func setData(data: NSDictionary) -> Void {
        self.result = data
        self.tableView.reloadData()
    }
    
    // for showing error alert message 
    func showAlert(msg: String) -> Void
    {
        networkStatus = "Failed"
        self.tableView.reloadData()
        
        let alertController = UIAlertController(title: "Error", message: msg, preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertController.addAction(defaultAction)
        presentViewController(alertController, animated: true, completion: nil)
        
        
    }
   
//override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool){
//    self.tableView.reloadData()
//    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
