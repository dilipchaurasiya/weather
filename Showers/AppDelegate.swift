//
//  AppDelegate.swift
//  Showers
//
//  Created by dilip chaurasiya on 5/13/16.
//  Copyright © 2016 dilip chaurasiya. All rights reserved.
//

import UIKit
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        let mainTable = self.window?.rootViewController as! MainTableViewController
        
        // To show network avtivity while data is being loaded. as soon as data is recieved value should be set false
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        // Yahoo weather api request url
        let url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22mumbai%22)%20and%20u%3D%22c%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"
        
        // Alamofire library for networking activities
        Alamofire.request(.GET,url
            ).responseJSON{ response in switch response.result {
                
            case .Success(let JSON):
            // Sending response result to MainTableViewController using setData method of MainTableViewController
            mainTable.setData(JSON as! NSDictionary)
                
            case .Failure(let error):
                if(error.code == -1009)
                {
                    // if there is error and data could not be loaded showing alert using following method
                    mainTable.showAlert("The Internet connection appears to be offline.")
                }
                else
                {
                    print("Request failed with error: \(error)")
                }
              
                mainTable.showAlert("Request failed with error.")
                
                }
                
                // network activity is complete
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
       
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

