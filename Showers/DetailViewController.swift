//
//  DetailViewController.swift
//  Showers
//
//  Created by dilip chaurasiya on 5/15/16.
//  Copyright © 2016 dilip chaurasiya. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController {

    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var high: UILabel!
    @IBOutlet weak var low: UILabel!
    
    @IBOutlet weak var weatherImage: UIImageView!
    
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var visibility: UILabel!
    
    @IBOutlet weak var speedView: UILabel!
    @IBOutlet weak var chillLabel: UILabel!
    
    @IBOutlet weak var atmosphereView: UIView!
    @IBOutlet weak var atmosphereLabelView: UIView!
    
    @IBOutlet weak var windView: UIView!
   
    
    var forcast: NSDictionary!       // dictionary containing forecast data
    var atmosphere: NSDictionary!    // dictionary containing atmosphere data for current date
    var wind: NSDictionary!          // dictionary containing wind data for current date
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLabelView(status)
        setLabelView(high)
        setLabelView(low)
        setLabelView(humidity)
        setLabelView(pressure)
        setLabelView(visibility)
        setLabelView(speedView)
        setLabelView(chillLabel)
        setData()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // for customizing uilabel appearance
    func setLabelView(label: UILabel) ->Void
    {
        label.layer.borderWidth = 1.0
        label.layer.cornerRadius = label.frame.size.height/2
        
    }
    
    //for setting value of each element in view
    func setData() ->Void
    {
    let imagecode: String = forcast.objectForKey("code") as! String
    self.weatherImage?.sd_setImageWithURL(NSURL(string: "http://l.yimg.com/a/i/us/we/52/"+imagecode+".gif"), placeholderImage: UIImage(named: "placeholderImage"))
        
        day.text = forcast.objectForKey("day") as? String
        date.text = forcast.objectForKey("date") as? String
        status.text = forcast.objectForKey("text") as? String
        high.text = (forcast.objectForKey("high") as? String)!+"\u{00B0}"
        low.text = (forcast.objectForKey("low") as? String)!+"\u{00B0}"
        
        //if current date selected show atmosphere conditions otherwise hide atmosphere View
        if(atmosphere != nil)
        {
            atmosphereLabelView.hidden = false
            atmosphereView.hidden = false
            humidity.text = (atmosphere.objectForKey("humidity") as? String)!+"%"
            
            pressure.text = atmosphere.objectForKey("pressure") as? String
            
            visibility.text = atmosphere.objectForKey("visibility") as? String
        }
        
        //if current date selected show wind conditions otherwise hide wind View
        if(wind != nil)
        {
            windView.hidden = false
            chillLabel.text = wind.objectForKey("chill") as? String
            speedView.text = (wind.objectForKey("speed") as? String)!+" Km/h"
        }
    }
    
    @IBAction func doneClicked(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
